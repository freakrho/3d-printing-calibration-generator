import json
import pathlib
import subprocess
import argparse
import os

from gcode_injector import GCodeInjector
from injection_rules import *


CONFIG = "config"
TEMP = "tmp"


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Generate calibration gcode")
    parser.add_argument("type", action='store', nargs=1, type=str, choices=['retraction', 'firstlayer', 'temperature'])
    parser.add_argument("-g", '--gcode', type=pathlib.Path)
    args = parser.parse_args()
    test_type = args.type[0]

    if not os.path.isdir(TEMP):
        os.mkdir(TEMP)

    with open(os.path.join(CONFIG, 'printer_data.json')) as temperature_data_file:
        printer_data = json.load(temperature_data_file)

    if test_type == "retraction":
        with open(os.path.join(CONFIG, 'retraction-data.json')) as temperature_data_file:
            temperature_data = json.load(temperature_data_file)
        model_data = temperature_data["model"]
        if args.gcode is not None:
            # Do gcode injection
            injector = GCodeInjector(args.gcode)
            layer = model_data["base_layers"] + 1
            for step in temperature_data["steps"]:
                retraction = step["retraction"]
                recover = step["recover"]
                injector.add_rule(RetractionRule(
                    layer,
                    retraction["distance"],
                    retraction["speed"],
                    retraction["z_lift"],
                    retraction["swap_length"]
                ))
                injector.add_rule(RecoverRule(
                    layer,
                    recover["distance"],
                    recover["speed"],
                    recover["z_lift"],
                    recover["swap_length"]
                ))
                layer += model_data["tower_step_layers"] + model_data["tower_ridge_layers"]
            injector.inject_rules()
        else:
            # Do model rendering
            print("Retraction")
            # Retraction
            outfile = os.path.join(TEMP, 'retraction.stl')

            if os.path.isfile(outfile):
                os.remove(outfile)

            arguments = ["openscad", f"-o{outfile}"]
            for key in model_data.keys():
                arguments.append(f"-D {key}={model_data[key]}")
            arguments.append(os.path.join(CONFIG, "retraction.scad"))
            print(f"Creating stl file: {' '.join(arguments)}")
            subprocess.run(arguments)
            print(f"Slice the file '{outfile}' using your slicer and run "
                  f">> {parser.prog} retraction -g \"path/to/file.gcode\"")
    elif test_type == "firstlayer":
        with open(os.path.join(CONFIG, 'firstlayer_data.json')) as temperature_data_file:
            firstlayer_data = json.load(temperature_data_file)
        outfile = os.path.join(TEMP, 'firstlayer.stl')
        arguments = ["openscad", f"-o{outfile}", f"-D bed_width={printer_data['bed_size_x']}",
                     f"-D bed_height={printer_data['bed_size_y']}", f"-D margin={printer_data['margin']}"]
        for key in firstlayer_data.keys():
            arguments.append(f"-D {key}={firstlayer_data[key]}")
        arguments.append(os.path.join(CONFIG, "first_layer.scad"))
        print(f"Creating stl file: {' '.join(arguments)}")
        subprocess.run(arguments)
    elif test_type == "temperature":
        with open(os.path.join(CONFIG, 'temperature_data.json')) as temperature_data_file:
            temperature_data = json.load(temperature_data_file)
        model_data = temperature_data["model"]

        if args.gcode is not None:
            injector = GCodeInjector(args.gcode)
            layer = model_data["base_layers"] + 1
            layer_step = model_data["step_layers"]
            step_data = temperature_data["step"]
            temp = step_data["temperature_start"]
            temp_increment = step_data["temperature_increment"]
            for i in range(0, model_data["steps"]):
                injector.add_rule(TemperatureRule(layer, temp))
                layer += layer_step
                temp += temp_increment
            injector.inject_rules()
        else:
            # Do model rendering
            print("Temperature")
            # Retraction
            outfile = os.path.join(TEMP, 'temperature.stl')

            if os.path.isfile(outfile):
                os.remove(outfile)

            arguments = ["openscad", f"-o{outfile}"]
            for key in model_data.keys():
                arguments.append(f"-D {key}={model_data[key]}")
            arguments.append(os.path.join(CONFIG, "temperature.scad"))
            print(f"Creating stl file: {' '.join(arguments)}")
            subprocess.run(arguments)
            print(f"Slice the file '{outfile}' using your slicer and run "
                  f">> {parser.prog} temperature -g \"path/to/file.gcode\"")
