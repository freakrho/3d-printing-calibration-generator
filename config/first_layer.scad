square_size = 20;
bed_width = 220;
bed_height = 220;
layer_height = 0.2;
margin = 10;

grid_width = 3;
grid_height = 3;
add_middle = false;

show_bed = false;

distance_x = (bed_width - square_size * grid_width - margin * 2) / (grid_width - 1);
distance_y = (bed_height - square_size * grid_height - margin * 2) / (grid_height - 1);
for (i=[0:grid_width-1]) {
	for(j=[0:grid_height-1]) {
		translate([
			margin + (distance_x + square_size) * i,
			margin + (distance_y + square_size) * j,
			0])
			cube([square_size, square_size, layer_height]);
	}
}

if (add_middle) {
	translate([
		(bed_width - square_size) / 2,
		(bed_height - square_size) / 2,
		0])
		cube([square_size, square_size, layer_height]);
}

if (show_bed) {
	color([1, 0, 0, .5])
	translate([0, 0, -1])
		cube([bed_width, bed_height, 1]);
}