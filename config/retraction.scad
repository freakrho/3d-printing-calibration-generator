/* [Hidden] */
$fn = 100;
error = 0.01;

/* [Parameters] */
layer_height = .2;

/* [Base] */
base_width = 30;
base_depth = 10;
base_layers = 3;
base_roundness = 2;

/* [Towers] */
tower_diameter = 7;
tower_ridge_diameter = 5.5;
tower_step_layers = 10;
tower_ridge_layers = 3;
tower_steps = 7;
tower_padding = 4;

module tower() {
    for (i=[0:tower_steps - 1]) {
        translate([0, 0, i * layer_height * (tower_step_layers + tower_ridge_layers)]) {
            cylinder(d=tower_diameter, h=layer_height * tower_step_layers, $fn=5);

            if (i < tower_steps - 1) {
                translate([0, 0, layer_height * tower_step_layers])
                    cylinder(d=tower_ridge_diameter, h=layer_height * tower_ridge_layers, $fn=5);
            }
        }
    }
}

union() {
    // Base
    base_height = base_layers * layer_height;
    translate([base_roundness, base_roundness, 0])
    hull() {
        cylinder(r=base_roundness, h=base_height);
        translate([base_width - base_roundness * 2, 0, 0])
            cylinder(r=base_roundness, h=base_height);
        translate([base_width - base_roundness * 2, base_depth - base_roundness * 2, 0])
            cylinder(r=base_roundness, h=base_height);
        translate([0, base_depth - base_roundness * 2, 0])
            cylinder(r=base_roundness, h=base_height);
    }

    // Towers
    translate([0, base_depth / 2, base_height]) {
    	translate([tower_padding, 0, 0])
            tower();

    	translate([base_width - tower_padding, 0, 0])
        rotate([0, 0, 180])
            tower();
    }
}
