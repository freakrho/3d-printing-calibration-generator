$fn = 100;
error = 0.01;

layer_height = 0.2;

steps = 5;
step_layers = 20;
width = 20;
depth = 10;
bridge_layers = 2;
bridge_length = 10;

base_padding = 4;
base_layers = 3;

height = step_layers * layer_height;
bridge_height = bridge_layers * layer_height;
bridge_width = width - depth - bridge_height;

module step() {
    translate([0, 0, height / 2])
    union() {
        diameter = min(bridge_width, height) * 2 - bridge_height;
        difference() {
            translate([depth / 2, 0, 0])
                cube([width, depth, height], center=true);

            translate([-bridge_length / 2, 0, height / 2 - diameter / 2 - bridge_height])
            rotate([90, 0, 0])
                cylinder(h=depth + error * 2, d=diameter, center=true);
            
            translate([0, 0, -bridge_height / 2 - error])
                cube([bridge_length, depth + error * 2, height - bridge_height + error], center=true);

            // Triangle
            translate([bridge_length / 2, 0, -height / 2 - bridge_height / 2])
            rotate([90, 0, 0])
            linear_extrude(height=depth + error * 2, center=true)
                polygon([
                    [-error, -error],
                    [diameter / 2, -error],
                    [-error, diameter / 2],
                ]);
            
            translate([bridge_width / 2 + error, -depth / 2 - error, -height / 2])
                cube([depth + bridge_height, depth + error * 2, height - bridge_height]);
        }
        // Cone
        cone_d = depth / 2;
        cone_h = height * 2 / 3;
        translate([bridge_width / 2 + depth  - cone_d / 2, 0, -height / 2])
            cylinder(d1=depth / 2, d2=0, h=cone_h);

        // ceiling support
        translate([(width - depth) / 2 - bridge_height, 0, 0])
        rotate([90, 0, 0])
        linear_extrude(height=depth + error * 2, center=true)
            polygon([
                [-error, 0],
                [depth + bridge_height, height / 2 - bridge_height + error],
                [-error, height / 2 - bridge_height + error],
            ]);
    }
}

union() {
    // base
    base_height = base_layers * layer_height;
    translate([0, 0, -base_height])
    hull() {
        translate([-width / 2 + depth / 2, -depth / 2, 0])
            cylinder(r=base_padding, h=base_height);
        translate([-width / 2 + depth / 2, depth / 2, 0])
            cylinder(r=base_padding, h=base_height);
        translate([width, 0, 0]) {
            translate([-width / 2 + depth / 2, -depth / 2, 0])
                cylinder(r=base_padding, h=base_height);
            translate([-width / 2 + depth / 2, depth / 2, 0])
                cylinder(r=base_padding, h=base_height);
        }
    }
    for (i=[0:steps-1]) {
        translate([0, 0, height * i])
            step();
    }
}