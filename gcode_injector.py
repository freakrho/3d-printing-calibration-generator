from injection_rules import *
from typing import List, Dict

LAYER = ";LAYER:"


class GCodeInjector:
    def __init__(self, filename):
        self._filename = filename
        with open(filename, "r") as f:
            self.lines = f.readlines()
        self.rules = {}  # type: Dict[int, List[InjectionRule]]

    def inject_rules(self):
        line_index = 0
        for rule_layer in self.rules.keys():
            while line_index < len(self.lines):
                line = self.lines[line_index]
                if line.startswith(LAYER):
                    layer = int(line[len(LAYER):])
                    if layer == rule_layer:
                        for rule in self.rules[rule_layer]:
                            self.lines.insert(line_index + 1, rule.line() + "\n")
                            print(f"Injecting \"{rule.line()}\" in layer {rule.layer} (line {line_index + 1})")
                        break
                line_index += 1
        with open(self._filename, "w") as f:
            f.writelines(self.lines)

    def add_rule(self, rule: InjectionRule):
        if rule.layer not in self.rules:
            self.rules[rule.layer] = [rule]
            return
        self.rules[rule.layer].append(rule)

    def add_temperature_rules(self, start_temp: int, temperature_step: int,
                              base_height: float, layer_height: float, step_height: float, steps: int):
        layers_per_step = round(step_height / layer_height)
        base_layers = round(base_height / layer_height)
        temperature = start_temp
        for i in range(0, steps * layers_per_step, layers_per_step):
            self.add_rule(TemperatureRule(base_layers+i, temperature))
            temperature += temperature_step
