from codes import Marlin


class InjectionRule:
    def __init__(self, layer: int):
        self.layer = layer

    def line(self):
        return ""


class TemperatureRule(InjectionRule):
    def __init__(self, layer: int, temp: int):
        super().__init__(layer)
        self.temp = temp

    def line(self):
        return f"{Marlin.TEMPERATURE} S{self.temp}"


class RetractionRule(InjectionRule):
    def __init__(self, layer: int, distance: float, speed: float, z_lift: float, swap_length: float):
        super().__init__(layer)
        self._code = Marlin.RETRACT
        self.distance = distance
        self.speed = speed * 60  # convert mm/s to mm/m
        self.z_lift = z_lift
        self.swap_length = swap_length

    def line(self):
        t_line = self._code
        if self.speed > 0:
            t_line += f" F{self.speed}"
        if self.distance > 0:
            t_line += f" S{self.distance}"
        if self.swap_length >= 0:
            t_line += f" W{self.swap_length}"
        if self.z_lift > 0:
            t_line += f" Z{self.z_lift}"
        return t_line


class RecoverRule(RetractionRule):
    def __init__(self, layer: int, distance: float, speed: float, z_lift: float, swap_length: float):
        super().__init__(layer, distance, speed, z_lift, swap_length)
        self._code = Marlin.RECOVER
